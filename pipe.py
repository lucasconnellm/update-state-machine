import boto3
from bitbucket_pipes_toolkit import Pipe

schema = {
    'ASL_DEFINITION': {'type': 'string', 'required': True},
    'FUNCTION_NAME': {'type': 'string', 'required': True },

    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': False, 'default': 'us-east-1'},

    'FUNCTION_ALIAS': {'type': 'string', 'required': False, 'default': ''},
}

class StepFunctionUpdate(Pipe):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client = boto3.client(
            'stepfunctions',
            aws_access_key_id=self.get_variable('AWS_ACCESS_KEY_ID'),
            aws_secret_access_key=self.get_variable('AWS_SECRET_ACCESS_KEY'),
            region_name=self.get_variable('AWS_DEFAULT_REGION'),
        )

    def run(self):
        try:
            super().run()
            self.log_info('retrieving definition')
            defn = self.getDefinitionString()

            fn_alias = self.get_variable('FUNCTION_ALIAS')
            if fn_alias:
                self.log_info('inserting alias to definition string')
                defn = defn.format(FUNCTION_ALIAS=fn_alias)
            
            self.log_info('updating definition')
            rtn = self.client.update_state_machine(
                stateMachineArn=self.get_variable('FUNCTION_NAME'),
                definition=defn,
            )

            self.log_info(rtn)

            self.log_info('done!')
        except Exception as e:
            self.log_error('Error uploading: {}'.format(e))
            self.fail('exiting')

    def getDefinitionString(self):
        filepath = self.get_variable('ASL_DEFINITION')
        with open(filepath, 'r') as f:
            return f.read()


my_pipe = StepFunctionUpdate(schema=schema)
my_pipe.run()
