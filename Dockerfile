FROM python:3.7.5-alpine3.10

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY pipe.py /

ENTRYPOINT [ "python" ]
CMD [ "-u", "pipe.py" ]